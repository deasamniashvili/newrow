let number = 123;

const numToArr = (num) => {
    let arr = [];
    while (num && num > 0) {
        let remainder = num % 10; //3 //2 //1
        num = Math.floor(num / 10); //12 //1
        arr.push(remainder);
    }
    return arr.reverse();
}

let arrFromNum = numToArr(number);
console.log(arrFromNum);

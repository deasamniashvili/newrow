const promiseFunc = (number) => {
    let myPromise = new Promise((resolve, reject) => {
        (number > 10) && resolve("Number is more than 10.");
        reject('Number is not more than 10.');

    });
    myPromise.then(success => console.log(success))
        .catch((err) => console.log(err))
};

promiseFunc(91);
promiseFunc(10);
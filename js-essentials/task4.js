let obj1 = {
    a: 1,
    b: 2
};

let obj2 = {
    b: 3,
    c: 4
}

const mergeObjs = (ob1, ob2) => {
    let obj3 = {
        ...ob1,
        ...ob2
    };
    return obj3;
}

let newVal = mergeObjs(obj1, obj2);
console.log(newVal);
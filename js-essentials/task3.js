let myObj = {
    Name: 'Denis',
    Country: 'Israel'
}

let objToReplace = {
    Name: 'Nir'
    // Name: ''
}

const rename = (ob1, ob2) => {
    let ob3 = {
        ...ob1
    };
    if (!ob2.isEmpty) {
        for (const key in ob2) {
            if (ob3.hasOwnProperty(key) && ob2[key].length) {
                const element = ob3[key];
                let newKey = ob2[key];

                ob3[newKey] = element;
                delete ob3[key];
                return ob3;
            }
            return 'object is empty';
        }
    }
    console.log('please enter vaLid value');
    return ob3;
}

const newObject = rename(myObj, objToReplace);
console.log(newObject);
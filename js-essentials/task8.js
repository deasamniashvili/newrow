let arr = [1, 2, 3];
let [one, two, three] = arr;

let arr1 = [1, 2, 3, 5, 6];
let [num1, num2, ...rest] = arr1;


let obj = {
   name: 'Dea',
   country: 'Georgia'
}

const param = (object, newrow) => {
   let newObj = object;
   if (!object.payload) {
      object.payload = {};
   }

   newObj.payload.newrow = newrow;
   return {
      ...newObj
   }
}

console.log(param(obj, '_newrow'));


let arrOfObjs = [
    {a:1,b:2},
    {b:3, c:4},
    {c:5, d:6},
    {b:3, c:9},
    {e:5, d:9},
];


const mergeArr = (arr) => {
  let newObj;
  newObj = Object.assign({}, ...arr);
  return newObj;
}


let newObject = mergeArr(arrOfObjs);
console.log(newObject);

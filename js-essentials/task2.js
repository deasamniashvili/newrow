// 2.
let person = {
    name: 'Denis',
    country: 'Israel'
}

const toEditKey = (obj1, obj2) => {
    let newObject = {
            ...obj1,
            ...obj2
    }
    return newObject;
}

let myObj = toEditKey(person, { name: 'Nir', age: 20, gender: 'male' })
console.log(myObj);


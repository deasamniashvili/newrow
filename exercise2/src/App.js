import React, { Component } from 'react';
import ParticipantsContainer from './components/ParticipantsContainer';

export default class App extends Component {
  render() {
    return (
      <div>
         <ParticipantsContainer />
      </div>
    )
  }
}


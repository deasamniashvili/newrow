import React from 'react';
import star from '../star.png';


const PerParticipant = (props) => (
    <li>
      {props.currentUserId &&  <img src={star} alt='star' /> }
      {props.participant}
    </li>
);


export default PerParticipant;
import React, { Component } from 'react';
import '../styles/style.css'
import ParticipantsList from './ParticipantsList';
import NRSDK from '../lib/SDK';


const newUserJoined = 'newUserJoined';
const userConnected = 'userConnected';
const userDisconnected = 'userDisconnected';
const roomID = 'sdj-973';

export default class ParticipantsContainer extends Component {
    state = {
        participants: {},
        currentUserId: null
    }

    componentWillUnmount() {
        this.sdkSession.off(newUserJoined, this.userJoinedHandler);
        this.sdkSession.off(userConnected, this.userConnectedHandler);
        this.sdkSession.off(userDisconnected, this.userDisconnectedHandler);
    }

    componentDidMount() {
        this.sdkSession = NRSDK.initSession('key', roomID);

        this.sdkSession.on(newUserJoined, this.userJoinedHandler);
        this.sdkSession.on(userConnected, this.userConnectedHandler);
        this.sdkSession.on(userDisconnected, this.userDisconnectedHandler);
    }

    userJoinedHandler = ({user_id, full_name}) => {
        let newUser = { user_id, full_name };

        this.setState(prevState => ({
            participants: {
                ...prevState.participants,
                [user_id]: newUser
            },
        }))
    }

    userConnectedHandler = (user) => {
        let id = user.user_id;
        let name = user.full_name;
        let newUser = { user_id: id, full_name: name };

        this.setState(prevState => ({
            participants: {
                ...prevState.participants,
                [id]: newUser
            },
            currentUserId: id
        }))
    }

    userDisconnectedHandler = (userDIsconnected) => {
        this.setState({
            participants: Object.keys(this.state.participants).filter((user) => user.user_id !== userDIsconnected)
        });
    }

    render() {
        return (
            <ParticipantsList participants={this.state.participants} currentUserId = {this.state.currentUserId} />
        )
    }
}

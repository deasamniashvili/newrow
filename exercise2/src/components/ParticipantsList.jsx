import React, { Component } from 'react';
import PerParticipant from './PerParticipant';
// import SelfParticipant from './SelfParticipant';


class participantsList extends Component {
  render() {
    return (
      <div className="wrapper">
        <header>
          <h1>Users' List</h1>
        </header>
        <div className='list-type2'>
          {Object.keys(this.props.participants).length ?
            <ul>
              {Object.keys(this.props.participants).map((p) => (
                <PerParticipant
                  key={this.props.participants[p].user_id}
                  participant={this.props.participants[p].full_name}
                  currentUserId={this.props.currentUserId === this.props.participants[p].user_id}
                />
              ))}
              </ul>
              :
              // <div className="lds-ellipsis">Loading...<div></div><div></div><div></div><div></div></div>
              <h3>Loading...</h3>
              }
            
        </div>
      </div>
        )
      }
    }
    export default participantsList;
